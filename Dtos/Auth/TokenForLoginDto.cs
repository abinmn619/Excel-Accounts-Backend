namespace Excel_Accounts_Backend.Dtos.Auth
{
    public class TokenForLoginDto
    {
        public string auth_token { get; set; }
    }
}