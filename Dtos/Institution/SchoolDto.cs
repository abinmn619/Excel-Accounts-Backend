namespace Excel_Accounts_Backend.Dtos.Institution
{
    public class SchoolDto
    {
        public string Name { get; set; }
        public string District { get; set; }     
    }
}