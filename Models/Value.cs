namespace Excel_Accounts_Backend.Models
{
    public class Value
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}